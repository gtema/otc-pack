from st2common.runners.base_action import Action

import openstack


__all__ = [
    'SetContainerProperties'
]


class SetContainerProperties(Action):
    def run(self, **kwargs):
      properties = kwargs.pop('property', [])
      container = kwargs.pop('container')
      cloud = self.config['clouds']['OS_CLOUD']
      self.logger.info('Trying to set properties %s on the container %s in %s' % (properties, container, cloud))
      conn = openstack.connect(cloud)
      if conn:
          headers = {}
          for prop in properties:
            k, v = prop.split('=')
            headers[k] = v
          self.logger.info('headers=%s' % headers)

#          cont = conn.object_store.get_container_metadata(container=container)
#          self.logger.info('current container=%s' % cont)

          conn.object_store.set_container_metadata(container, **headers)
      return True
