from st2reactor.sensor.base import PollingSensor

import json

import openstack


class ServersCountSensor(PollingSensor):

    """
    * self.sensor_service
        - provides utilities like
            get_logger() for writing to logs.
            dispatch() for dispatching triggers into the system.
    * self._config
        - contains configuration that was specified as
          config.yaml in the pack.
    * self._poll_interval
        - indicates the interval between two successive poll() calls.
    """

    def __init__(self, sensor_service, config,
                 poll_interval=5):
        super(ServersCountSensor, self).__init__(
            sensor_service=sensor_service,
            config=config,
            poll_interval=poll_interval
        )
        self._conn = None
        self._logger = None


    def setup(self):
        # Setup stuff goes here. For example, you might establish connections
        # to external system once and reuse it. This is called only once by the system.
        self._logger = self.sensor_service.get_logger(name=self.__class__.__name__)
        cloud = self._config['clouds']['OS_CLOUD']
        self._logger.info('connecting to %s' % cloud)
        self._conn = openstack.connect(cloud)

    def poll(self):
        if self._conn:
            self._logger.debug('Connection is there. Look for something')
            containers = self._conn.list_containers()
            data = dict((s['name'], s['count']) for s in containers)
            old_data = self.sensor_service.get_value('otc_pack.containers') or '{}'
            old_data = json.loads(old_data)
            self._logger.info('old_data=%s'% old_data)
            if data != old_data:
                self._logger.info('old_data=%s, data=%s, cmp=%s' % (type(old_data), type(data), sorted(old_data) != sorted(data)))
                payload = {'containers': containers}
                self.sensor_service.dispatch(trigger='otc_pack.event1', payload=payload)
                self.sensor_service.set_value('otc_pack.containers', json.dumps(data))
        else:
            self._logger.error('connection is not available')

    def cleanup(self):
        # This is called when the st2 system goes down. You can perform cleanup operations like
        # closing the connections to external system here.
        pass

    def add_trigger(self, trigger):
        # This method is called when trigger is created
        pass

    def update_trigger(self, trigger):
        # This method is called when trigger is updated
        pass

    def remove_trigger(self, trigger):
        # This method is called when trigger is deleted
        pass
